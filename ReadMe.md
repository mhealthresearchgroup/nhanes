### Table of contents

You can insert a table of contents using the marker `[TOC]`:

[TOC]


README
===
 Please follow the steps below to create and run your own AWS environment for the NHANES
 project.

 LEGEND:
 >> means terminal commands
 * means general information/instructions
 ** means important information/instructions


First things first!
===
 1. Please obtain you AWS credentials from NIH (AWS_ACCESS_KEY and AWS_SECRET). You WILL need these.
 2. For the sake of testing this, we cannot request for a separate key for everyone
    so please email me for the keys. I'll send them to you (If I haven't already).


Requirements
===
 * AWS Account
   Please follow the link below and sign-up for a new account if you don't already have one:
   https://aws.amazon.com/
 * Windows, MacOS or Linux machine with administrator privileges.
 * Local installation of NodeJS (Installation instructions below)
 * curl or wget (Installation instructions below)
 * aws-cli (Installation instruction below)


After Creating AWS Account (One Time Activity)
===
 * Login to your AWS account by going to 'aws.amazon.com' and clicking on 'sign-in to console'
   On the top right of the page. Enter your credentials to login.
 * In the search-bar type 'IAM' and press enter.
 * Click on the link 'Users' under 'IAM RESOURCES'
 * Click on your username.
 * Go to the tab 'Security Credentials'.
 * Click on 'Create Access Key'
 * Click on 'Download .csv file' to download the '.csv' file. Make sure you know where the file
   was downloaded.
 * This is your 'PERSONAL CREDENTIALS FILE'. We will need it in further steps


Creating a key pair and security group (One Time Activity)
Optional: For running on your own account only.


Skip this section for now - (Sandarsh - 13 December 2017)
===
 Creating a Key Pair
 * Go to the AWS home screen. (Click on the AWS logo on the top left).
 * Enter 'EC2' in the search box and click on EC2.
 * In the menu to the left, look for the option 'Key Pairs' and click on it.
 * Click on 'Create Key Pair', give it an appropriate name and hit create.
 * Select the appropriate folder to save your key pair in. Remember the location. Eg your_home_folder. Click 'save'.
 * P.S. You can ssh into your Web Server using this key.

 Creating a Security Group
 * In the menu to the left, look for the option 'Security Groups' and click on it.
 * Click on 'Create Security Group', give an appropriate name and description.
 * You will see two tabs 'Inbound' and 'Outbound' on the screen as well.
 * Click on 'Inbound' and then click on 'Add Rule'.
 * Change 'Type' to 'Custom TCP Rule'.
 * Change 'Port Range' to '3000'.
 * Change 'Source' to 'Anywhere'.
 * Click on 'Outbound' tab and then click on 'Add Rule'.
 * Change 'Type' to 'All Traffic'.
 * Change 'Port Range' to '0 - 65535'.
 * Change 'Destination' to 'Anywhere'.
 ** At the end of this, you should have just one rule in both inbound and outbound tabs configured as per above.
 * Click on create.
 ** Please note the name of the security group you just created.


#### So far so good!
 Right now, you should be in possession of two sets of keys, one for your own account which
 you created in the above steps and one for the NIH account which you obtained from them
 directly or from me.
 If yes, carry on. If not, please complete the above steps or reach out to me.

-------
For Macintosh machines:
===
 * Press 'command + space' and type 'terminal' in the dialog box to open terminal, enter.
 * Copy the following terminal commands (excluding '>>') one by one and hit enter:
```sh
 >> cd ~; mkdir nhanes; cd nhanes
```

 * You can replace nhanes above to some name you are comfortable with. This is the name of
 * the folder that will be created for your project. Please make sure you change it in both
 * occurrences above.
```sh
 >> curl -o init.sh https://s3.amazonaws.com/sandarsh-scripts/init.sh
 >> chmod 755 init.sh
 >> ./init.sh
```
 * When asked for 'password:', enter your computer's admin password.
 **Please read the prompts on the terminal carefully for the below sections.

 Configuring Your Personal Account
 * When asked for "AWS Access Key ID", "AWS Secret Access Key", copy the
   credentials from the 'PERSONAL CREDENTIALS FILE' (Cell 2A and 2B) in the above section and paste
   them here for corresponding fields.
 * Enter 'us-east-1' for Default Region.
 * Press Enter for Default Format (Leave it blank).

 Configuring the NIH Account:
 * When asked for "AWS Access Key ID", "AWS Secret Access Key", copy the
   credentials from the 'NIH CREDENTIALS FILE' that was sent to you.
 * Enter 'us-west-2' for Default Region.
 * Press Enter for Default Format (Leave it blank).
 * Don't forget the dot (.) in the below command.
```sh
 >> open .
```

 * The open command should open a GUI window with your files downloaded for starting the
   environment.
 * Please do not close the terminal window.
 * Enter the 'dist' folder.
 * Open the file 'config.js' in any text/code editor.
 * Keep your 'NIH CREDENTIALS FILE', 'security group name' and 'key file name' handy.
 Note : If you don't have the 'security group' or your 'key file name'. Leave the default
        values filled in.

 * Please update the 'config.js' (read comments in the file) as per your own environment.
 * Save the file after you're done updating (command + s). Now run the below in the terminal.
```sh
 >> chmod 777 dist/init.js; chmod 777 dist/public; cd dist
 >> node init.js &
```
 Your environment should be up in a few minutes.
 The DASHBOARD LINK let's you delete your server. CAUTION : It does not delete your job machines in the back end.
 The PORTAL LINK is where you run your jobs from.

 If there are any errors, your build will stop. Please contact me immediately if that happens.
 We will need to verify if something actually ran and will need to shut it down, or we pay, literally.


#### To bring up the web server again (except for the first time)

    CAUTION : Do not bring up an environment twice, it will not behave as expected.
    A good test is to visit http://localhost:35790 to check if the page is active.
    If it is, this means that your environment is running.
    If not, you can bring up the environment again by doing the following in the terminal.

 >> node ~/nhanes/dist/init.js
 ** Make sure the name 'nhanes' above is the same as you put in the first command in the
    previous section.

----------

For Windows machines
===
### Setup

#### Python
1. Install Python if not installed from https://www.python.org/downloads/release/python-362/
2. Verify you have installed correct version by typing in [command prompt](#start-command-prompt)  "python"

#### AWS CLI
1. Download 64-bit version from https://s3.amazonaws.com/aws-cli/AWSCLI64.msi
2. Open [command prompt](#start-command-prompt) and type `aws configure`
3. Put in AWS Access ID and Keys as found above and set region as us-west-2
4. Verify by typing in command prompt `aws s3 ls s3://pam-data`

#### Node.js
1. Install Node.js from https://nodejs.org/dist/v8.9.3/node-v8.9.3-x64.msi
2. Verify you have installed correct version by typing `node -v` in [command prompt](#start-command-prompt)

#### Start Command Prompt
1. Press `windows button`
2. Type `cmd`
3. Right click  `Command Prompt` and select `Run as administrator`
4. Copy the following commands one by one and enter details as asked, if you only have ONE set of keys then enter the same in both fields.

```cmd
cd C:\ && mkdir nhanes && cd nhanes && mkdir dist && cd dist
aws configure
```
> AWS Access Key ID: ENTER **PERSONAL** KEY ID
> AWS Secret Access Key: ENTER **PERSONAL** SECRET ACCESS KEY
> Default Region: us-west-2
> Default output format: table

```cmd
aws configure --profile nih
```
> AWS Access Key ID: ENTER **NIH** KEY ID
> AWS Secret Access Key: ENTER **NIH** SECRET ACCESS KEY
> Default Region: us-west-2
> Default output format: table

```cmd
aws s3 cp s3://neu-packaging-dist/dist . --recursive --profile nih
npm install
write config.js
```
5.  This opens up wordpad and where you need to change all the `...` as asked in the comments file
6. Save the file `config.js`
7. Returning back to prompt type `node init.js`
8. Once you see the Dashboard link come up click [http://localhost:35790](http://localhost:35790), you should now see something like following image
9. ![Local dashboard](https://i.imgur.com/RgWsb79.png)
10. Your main environment should be up in a few minutes and the *Server Status* would change to **Up** in green color
11. ![Local dashboard Up](https://i.imgur.com/gKAISs9.png)
12. Above dashboard has a table of all jobs that have been run in this session as well has an option to Terminate Cloud which will bring the whole system down.
 >The **DASHBOARD LINK** lets you delete your server. **CAUTION** It does not delete your job machines in the back end.
>The **PORTAL LINK**(`...us-west-2-compute.amazon....`) is where you run your jobs from.

#### How to shutdown system
> To **SHUTDOWN** the system, goto command prompt and press `Ctrl + C` or click `Terminate Cloud` button on the dashboard which will enable only when all the jobs are finished.

#### To bring up the web server again (except for the first time)

>**CAUTION** : Do not bring up an environment twice, it will not behave as expected.
    A good test is to visit [http://localhost:35790](http://localhost:35790) to check if the page is active.
    If it is, this means that your environment is running.
    If not, you can bring up the environment again by doing the following in the terminal.

 **All steps till now are just one time configuration steps, to run the system again all you need to do is **
 1. Open [command prompt](#start-command-prompt)
 2. Copy paste following in command prompt, to shutdown press `Ctrl+C`
```cmd
cd C:\ && mkdir nhanes && cd nhanes && mkdir dist && cd dist && node init.js
```

#### S3Browser: Upload/Download data in S3 buckets
1. Download http://s3browser.com/
2. Open it, it should automatically pick up credentials from aws config and show you S3 buckets
3. If it does not pick up credentials and shows a window as below, please fill your AWS credentials (Access ID and Secret Key) just like earlier steps.
4. ![s3 browser: add new account](https://i.imgur.com/ZUayKPz.png)
4. You should now see all the buckets and can even create your own buckets or download data of a job.
5. ![S3Browser All NHANES buckets](https://i.imgur.com/szNdmUW.png)


### How to run a job
Upon completion of all the above steps and clicking on **Portal URL** link as shown below
![Dashboard Portal URL link](https://i.imgur.com/BrGzbf9.png)
1. Clicking on the above link will take us to a AWS dashboard that will let us run our jobs, on the left are all the action from top down manner and right is the execution window which tells you active status of the job that you run.
2. ![AWS Dashboard](https://i.imgur.com/twWmddZ.png)

#### How to run multiple jobs
> To run multiple jobs, all you need to do is open another tab of the AWS dashboard by clicking on the Portal URL link from local dashboard or just copying URL or AWS dashboard

#### Upload your algorithm
1. Your algorithm should be zipped (.zip), if you are not sure how to do that [Click this](https://www.laptopmag.com/articles/how-to-zip-files-windows-10)
2. Click on *Choose File* button and select your file
3. Type in the name of the main entry point in your algorithm
4. Click *Upload*
5. The execution logs pane on right should show the status as *Upload Successful*
6. If that fails, contact administrator.

#### Select Bucket and Machine Type
Bucket is a remote location in AWS S3 that contains your data.
1. A S3 bucket would automatically be selected for you by default but if you are using a custom dataset, you can select that via drop-down menu.
2. Next up, you have to select a machine type (t2 vs m4) , m4 are durable for long jobs and powerful while t2 are cheap and should only be used for testing if everything works fine, We have **m4.large** selected as default.

#### Baseline
Before we run jobs on full datasets, we run a sample job to baseline our cost and timing estimates
1. We let the number of machines and files remain equal to 1.
2. Click on *Run baseline and cost estimate*
3. You can track the progress of jobs in the right pane, if there are any errors the number 0 in the title of red *Execution Errors* pane would change and you can click on it to view those errors.

#### Full dataset Run
If the above **baseline** is successful, you should see your estimates in the left pane.
1. From here on to run a custom job, you just need to upload your algorithm, specify number of machines and files.
2. Click on *Run Custom Job*, track progress
3. Once completed, take a note of the **jobID** printed in the execution window at the start of the execution logs pane.
4. Fire up *S3Browser* and browse to bucket name **nhanes-data-output** where you will find a directory by the name of the **jobID** which will have all your concatenated files.
> Before *Run Custom Job* button, you should see following two buttons that mean:
> Concatenate: One `csv` per dataset (person)
> Concatenate All: One `csv` for all the datasets and a single header in mHealth format





----------


[WIP] For Linux machines
===
Testing under progress
