function httpGet(theUrl)
{
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    let result = JSON.parse(xmlHttp.responseText);
    document.getElementById("instanceId").innerHTML = result.serverId;
    document.getElementById("queueUrl").innerHTML = result.queueUrl;
    document.getElementById("portalUrl").innerHTML = result.portalUrl;
}

function terminateAll(){
    let http = new XMLHttpRequest();
    let url = "http://localhost:35790/terminate";
    let params = {
        terminationId : 666
    };
    http.open("POST", url, false);

//Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/json");
    http.send(JSON.stringify(params));
}