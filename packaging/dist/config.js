// {
// 	"__comments" : {
// 		"1" : "Please do not change the data on the left. The fields to the right are editable",
// 		"2" : "The primary_account_access_id and primary_account_access_secret are from your CREDENTIALS FILE. Please copy them from there",
// 		"3" : "If the S3 bucket for your data-set resides in a different account (eg. Calibre/NIH) then you should have separate CREDENTIALS for them.",
// 		"4" : "If 3 is true and you do not have CREDENTIALS, please do not proceed and contact your supervisor",
// 		"5" : "If 3 is not true, then copy the CREDENTIALS from your CREDENTIALS FILE",
// 		"6" : "IF you are unsure about 3, please stop and contact your supervisor",
// 		"7" : "Completed steps 1-6? Please save and exit.",
// 		"nih_key" : "AKIAJEBYCPXTDOZMVQSQ",
// 		"nih_secret" : "BlHpSqfxh4KcWun16x7aFuJ/MZF4X9nvB3hKua3I",
// 		"my_key" : "AKIAI24GRI5P36YSBBPA",
// 		"my_secret" : "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"
// 	},
/*
	Please note : This is the configuration file. Any changes you make here will reflect the next time to initiate the
	environment
 */

/*
************************************************************************************************************************
Local parameters
************************************************************************************************************************
 */
let bitbucket_username = "sandarsh";
module.exports.dashboard_port = 35790;

/*
************************************************************************************************************************
Web server parameters
************************************************************************************************************************
 */
module.exports.web_server_account_access_id = "AKIAJEBYCPXTDOZMVQSQ";
module.exports.web_server_account_access_secret = "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";
module.exports.web_server_region = "us-west-2";
module.exports.web_server_ssh_keyName = "ndacs-spades";
module.exports.web_server_repo_url = "https://" + bitbucket_username + "@bitbucket.org/mhealthresearchgroup/nhanes.git";
module.exports.web_server_security_group = "web-server";
module.exports.web_server_subnet_id = "subnet-44f0ce0d";

/*
Valid Values:
t1.micro | t2.nano | t2.micro | t2.small | t2.medium | t2.large | t2.xlarge | t2.2xlarge | m1.small | m1.medium |
m1.large | m1.xlarge | m3.medium | m3.large | m3.xlarge | m3.2xlarge | m4.large | m4.xlarge | m4.2xlarge | m4.4xlarge |
m4.10xlarge | m4.16xlarge | m2.xlarge | m2.2xlarge | m2.4xlarge | cr1.8xlarge | r3.large | r3.xlarge | r3.2xlarge |
r3.4xlarge | r3.8xlarge | r4.large | r4.xlarge | r4.2xlarge | r4.4xlarge | r4.8xlarge | r4.16xlarge | x1.16xlarge |
x1.32xlarge | x1e.xlarge | x1e.2xlarge | x1e.4xlarge | x1e.8xlarge | x1e.16xlarge | x1e.32xlarge | i2.xlarge |
i2.2xlarge | i2.4xlarge | i2.8xlarge | i3.large | i3.xlarge | i3.2xlarge | i3.4xlarge | i3.8xlarge | i3.16xlarge |
hi1.4xlarge | hs1.8xlarge | c1.medium | c1.xlarge | c3.large | c3.xlarge | c3.2xlarge | c3.4xlarge | c3.8xlarge |
c4.large | c4.xlarge | c4.2xlarge | c4.4xlarge | c4.8xlarge | c5.large | c5.xlarge | c5.2xlarge | c5.4xlarge |
c5.9xlarge | c5.18xlarge | cc1.4xlarge | cc2.8xlarge | g2.2xlarge | g2.8xlarge | g3.4xlarge | g3.8xlarge | g3.16xlarge |
g1.4xlarge | p2.xlarge | p2.8xlarge | p2.16xlarge | p3.2xlarge | p3.8xlarge | p3.16xlarge | d2.xlarge | d2.2xlarge |
d2.4xlarge | d2.8xlarge | f1.2xlarge | f1.16xlarge
 */
module.exports.web_server_type = "t2.micro";
module.exports.web_server_ami = "ami-e689729e";


/*
************************************************************************************************************************
S3 bucket parameters parameters
************************************************************************************************************************
 */
module.exports.dataset_bucket_access_id = "AKIAJEBYCPXTDOZMVQSQ";
module.exports.dataset_bucket_access_secret = "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";
module.exports.dataset_bucket_region = "us-west-2";


/*
************************************************************************************************************************
Queue parameters
************************************************************************************************************************
 */
module.exports.queue_name = bitbucket_username + "-nhanes-data-queue";

