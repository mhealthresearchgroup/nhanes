let config = require("./config.js");
let AWS = require("aws-sdk");
let express = require('express');
let bodyParser = require('body-parser');
let app = express();
let instanceId = "";
let queueUrl = "";
let portalUrl = "";

AWS.config.update({
    accessKeyId : config.web_server_account_access_id,
    secretAccessKey : config.web_server_account_access_secret,
    region : config.web_server_region
});

let sqs = new AWS.SQS();
let ec2 = new AWS.EC2();

function terminateAll(callback){
    let params = {
        InstanceIds: [ /* required */
            instanceId
        ],
        DryRun: false
    };
    ec2.terminateInstances(params, function(err, data) {
        if (err) {
            console.log(err);
            console.log("Attempting to delete queue");
            let params = {
                QueueUrl: queueUrl
            };
            sqs.deleteQueue(params, function(err, data) {
                if (err) {
                    console.log("Could not delete queue because of an unknown error while cleaning up\n", err);
                    console.log("Your queue might still be active and you may be charged for it.");
                    console.log("Please contact the support team immediately..");


                } else {
                    console.log("Queue deleted successfully");
                    console.log("Infrastructure cleanup completed successfully.")
                }
            });
            console.log("Your web server could not be terminated and you might still be getting charged for it");

            console.log("Please contact support immediately");
        } else {
            // console.log(data);
            console.log("\nWeb server deletion successful");
            console.log("Attempting to delete queue");
            let params = {
                QueueUrl: queueUrl
            };
            sqs.deleteQueue(params, function(err, data) {
                if (err) {
                    console.log("Could not delete queue because of an unknown error while cleaning up\n", err);
                    console.log("Your queue might still be active and you may be charged for it.");
                    console.log("Please contact the support team immediately..");
                } else {
                    console.log("Queue deleted successfully");
                    console.log("Infrastructure cleanup completed successfully.")
                    callback(false, true);
                }
            });
        }
    });
}

app.get('/info', function(req, res){
    if(instanceId === "" || queueUrl === "" || portalUrl === ""){
        terminateAll();
    } else {
        res.json({
            serverId: instanceId,
            queueUrl: queueUrl,
            portalUrl : portalUrl
        })
    }
});

app.post('/terminate', bodyParser.json(), function(req, res){
    let body = req.body;
    if(body.terminationId === 666){
        terminateAll(function(err, success){
            if(err){
                res.sendStatus(400);
            } else {
                res.sendStatus(200);
                process.exit(0);
            }
        });
    } else {
        res.sendStatus(400);
    }
});

function init(){
    let listener = app.listen(
        config.dashboard_port ||
        getRandomInt(15000, 50000),
        () => {
            app.use(express.static('public'));

            let params = {
                QueueName: config.queue_name + ".fifo",
                Attributes: {
                    FifoQueue : 'true'
                }
            };
            console.log("Attempting to create a queue..");
            sqs.createQueue(params, function(err, data){
                ec2Init(err, data, listener)
            });
        });
}

function ec2Init(err, data, listener){
    if(err){
        console.log("Could not create a queue. Aborting...\n", err);
        process.exit(0);
    }
    queueUrl = data.QueueUrl;
    console.log("Queue creation successful on url : ",queueUrl);
    let configStr = "export AWS_ACCESS_KEY_ID=" + config.web_server_account_access_id + "\n";
    configStr += "export AWS_SECRET_ACCESS_KEY=" + config.web_server_account_access_secret + "\n";
    configStr += "export AWS_REGION=" + config.web_server_region + "\n";
    configStr += "export SQS_QUEUE_URL=" + queueUrl + "\n";
    let params = {
        ImageId : config.web_server_ami,
        SubnetId : config.web_server_subnet_id,
        InstanceType: config.web_server_type,
        MaxCount: 1, /* required */
        MinCount: 1, /* required */
        KeyName: config.web_server_ssh_keyName,
        TagSpecifications: [
            {
                ResourceType: 'instance',
                Tags: [
                    {
                        Key: 'Name',
                        Value: 'Web Server'
                    },
                    {
                        Key: 'createdBy',
                        Value: 'NEU'
                    }
                    /* more items */
                ]
            }],
        UserData : (new Buffer(
            "#!/bin/bash\n" +
            "yum update -y\n" +
            "echo \"" + configStr + "\" > /etc/profile\n" +
            "yum install -y gcc-c++ make\n" +
            "curl -sL https://rpm.nodesource.com/setup_6.x | sudo -E bash -\n" +
            "yum install -y nodejs\n" +
            "yum -y install git\n" +
            "git clone " + config.web_server_repo_url + "\n" +
            "npm --prefix /nhanes/front-end install\n" +
            "node /nhanes/front-end/server.js"
        ).toString('base64'))
    };
    ec2.runInstances(params, function(err, data){
        if (err) {
            console.log("\nError in creating server, aborting infrastructure build",err);
            console.log("Deleting queue at url : ", queueUrl);
            let params = {
                QueueUrl: queueUrl
            };
            sqs.deleteQueue(params, function(err, data) {
                if (err) {
                    console.log("Could not delete queue because of an unknown error while cleaning up\n", err);
                    console.log("Your queue might still be active and you may be charged for it.");
                    console.log("Please contact the support team immediately..");

                } else {
                    console.log("Queue deleted successfully");
                    console.log("Infrastructure cleanup completed successfully.")
                }
            });

        } else {
            instanceId = data.Instances[0].InstanceId;
            console.log("Successfully launched the web server. \nID = ", instanceId);
            params = {
                InstanceIds: [
                    instanceId
                ]
            };
            console.log("Attempting to fetch your public IP address");
            setTimeout(() => {
                ec2.describeInstances(params, function(err, data){
                    if (err){
                        console.log("Unexpected error. Could not fetch public ip of the web server");
                        console.log("Cancelling infrastructure build");
                        console.log("Attempting to terminate the web server server");
                        let params = {
                            InstanceIds: [ /* required */
                                instanceId
                            ],
                            DryRun: false,
                            Force: true
                        };
                        ec2.terminateInstances(params, function(err, data) {
                            if (err) {
                                console.log("Attempting to delete queue");
                                let params = {
                                    QueueUrl: queueUrl
                                };
                                sqs.deleteQueue(params, function(err, data) {
                                    if (err) {
                                        console.log("Could not delete queue because of an unknown error while cleaning up\n", err);
                                        console.log("Your queue might still be active and you may be charged for it.");
                                        console.log("Please contact the support team immediately..");

                                    } else {
                                        console.log("Queue deleted successfully");
                                        console.log("Infrastructure cleanup completed successfully.")
                                    }
                                });
                                console.log("Your web server could not be terminated and you might still be getting charged for it");
                                console.log("Please contact support immediately");
                            } else {
                                // console.log(data);
                                console.log("\nWeb server deletion successful");
                                console.log("Attempting to delete queue");
                                let params = {
                                    QueueUrl: queueUrl
                                };
                                sqs.deleteQueue(params, function(err, data) {
                                    if (err) {
                                        console.log("Could not delete queue because of an unknown error while cleaning up\n", err);
                                        console.log("Your queue might still be active and you may be charged for it.");
                                        console.log("Please contact the support team immediately..");
                                    } else {
                                        console.log("Queue deleted successfully");
                                        console.log("Infrastructure cleanup completed successfully.")
                                    }
                                });
                            }
                        });
                    } else {
                        portalUrl = "http://" + data.Reservations[0].Instances[0].PublicDnsName + ":3000";
                        console.log(data.Reservations[0].Instances[0].PublicIpAddress);
                        console.log("\n*************************************************************************************");
                        console.log("Dashboard link : http://localhost:" + listener.address().port);
                        console.log("Portal Link : http://" + data.Reservations[0].Instances[0].PublicDnsName + ":3000");
                        console.log("*************************************************************************************");
                        console.log("Please contact the system administrator if the url does not work in 3-5 mins.");
                        console.log("Your environment should be ready in a while...");
                        console.log("Navigate to 'Portal Link' to run your jobs.");
                        console.log("Navigate to 'Dashboard Link' to stop your environment.");
                        console.log("Infrastructure build complete.");
                    }
                })}, 10000);
        }
    })
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
}

init();
