#!/bin/bash

/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install node
brew install awscli
aws configure
mkdir dist
aws s3 sync s3://sandarsh-init ./dist
npm --prefix ./dist/ install
cd dist