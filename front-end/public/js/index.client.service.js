'use strict';

(function(){
    angular
        .module('NhanesAws')
        .factory('IndexService', IndexService);

    function IndexService($http){
        let api = {
            getDatasetList : getDatasetList,
            getInstanceTypeList : getInstanceTypeList,
            uploadCodeFile : uploadCodeFile,
            runBaseLine : runBaseLine,
            getPrice : getPrice,
            terminateExecution: terminateExecution,
            uploadLogs: uploadLogs,
            concatenateAll: concatenateAll
        };
        return api;

        function concatenateAll(jobId) {
            let params = {
                jobId: jobId
            }
            return $http.post('/concatenateAll', params);
        }
        function terminateExecution(jobId) {
            let params = {
                jobId: jobId
            }
            return $http.post('/terminateExecution', params);

        }

        function uploadLogs(jobId, logs) {
            let params = {
                jobId: jobId,
                logs: logs
            }
            return $http.post('/uploadLogs', params);
        }

        function getPrice(numMachines, numFiles, maxOneOffTime, pessimisticDownloadTime, pessimisticExecutionTime, pessimisticUploadTime, instanceType) {
            const priceList = {
                "t2.nano": 0.0058,
                "t2.micro": 0.0116,
                "t2.small": 0.023,
                "m4.large": 0.1,
                "m4.xlarge": 0.2
            };
            let pricePerMachine = priceList[instanceType];
            if(numFiles === "" || numMachines === ""){
                return 0;
            }
            let totalOneOff = maxOneOffTime;
            let totalDownloadTime = pessimisticDownloadTime * (numFiles/numMachines);
            let totalExecutionTime = pessimisticExecutionTime * (numFiles/numMachines);
            let totalUploadTime = pessimisticUploadTime * (numFiles/numMachines);
            let totalTime = totalOneOff + totalDownloadTime + totalExecutionTime + totalUploadTime;
            console.log(totalTime, pricePerMachine, numFiles);
            return (Math.ceil(totalTime/60/60) * pricePerMachine * numMachines);
        }



        function uploadCodeFile(formData){
            return $http.post('/codeUpload', formData);
        }

        function runBaseLine(body){
            return $http.post('/runBaseLine', body);
        }

        function getDatasetList(){
            return $http.get('/dataSetList');
        }

        function getInstanceTypeList(){
            return $http.get('/instanceTypeList');
        }
    }
})();
