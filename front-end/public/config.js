'use strict';

(function(){
  angular
  .module("NhanesAws")
  .config(Conf);

function Conf($routeProvider){
  $routeProvider
    .when('/', {
      templateUrl : 'views/index.html',
      controller : 'IndexController',
      controllerAs : 'model'
    })
}
})();
