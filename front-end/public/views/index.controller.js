'use strict';
let socket;

(function () {
    angular
        .module('NhanesAws')
        .controller('IndexController', ['$scope', 'Upload', 'IndexService', IndexController]);

    function updateScroll() {
        var element = document.getElementById("output-console");
        element.scrollTop = element.clientHeight + 500;
    }


    function IndexController($scope, Upload, IndexService) {
        let vm = this;
        let url = "s3://neu-algorithm-code/mims-unit.zip";
        // let url = "s3://neu-algorithm-code/218f821c2b7cc9f74f8447d1546f6f1f.zip";
        vm.entrypoint = "mims-unit.R";
        // vm.entrypoint = "checkIdleSleepModeFinal.py";
        vm.numOfMachines = 1;
        vm.numFiles = 1;
        vm.s3Cost = 0;
        vm.message = "Welcome to NHANES AWS project!!";
        vm.dataset = "nhanes-data-input-2";
        vm.instanceType = "t2.nano";
        vm.cost = 0.0;
        vm.approxExecTime = 0.0;
        vm.perDSConcat = true;
        vm.compDSConcat = false;
        vm.dataSetList = [];
        vm.instanceList = [];
        let viewArr = [];
        vm.jobList = {};
        let recorded = false;
        let firstPartialTime = 0;
        let numFiles = 1;
        let numMachines = 1;
        let machinesFailed = 0;
        let configuring = 0;
        let s3downloading = 0;
        let executing = 0;
        let s3uploading = 0;
        let finished = 0;
        let jobId = "";

        // Metrics for baseline calculation and extrapolation..
        let maxOneOffTime = 0;
        let maxdownloadTime = 0;
        let pessimisticDownloadTime = 0;
        let maxExecutionTime = 0;
        let pessimisticExecutionTime = 0;
        let maxUploadTime = 0;
        let pessimisticUploadTime = 0;
        ///////////////////////////////////////////////////////

        function diffInSecondsFromNow(ts) {
            let milliseconds = (new Date).getTime();
            let diff = (milliseconds - ts) / 1000;
            return diff;
        }

        function showView() {
            vm.message = "";
            let jobIdStr = "Job ID : " + jobId +
                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T : " + (vm.numOfMachines - machinesFailed) +
                "&nbsp;&nbsp;&nbsp;C : " + configuring +
                "&nbsp;&nbsp;&nbsp;S3D : " + s3downloading +
                "&nbsp;&nbsp;&nbsp;E : " + executing +
                "&nbsp;&nbsp;&nbsp;S3U : " + s3uploading +
                "&nbsp;&nbsp;&nbsp;F : " + finished;
            viewArr.splice(3, 1, jobIdStr)
            for (let element in viewArr) {
                vm.message += viewArr[element] + "<br>";
            }
        }

        let updateView = function (data, jobId, callback) {
            configuring = 0;
            s3downloading = 0;
            executing = 0;
            s3uploading = 0;
            finished = 0;
            let toKeep = viewArr[0];
            // toKeep = "Welcome to NHANES AWS project!!";
            viewArr = [];
            viewArr.push(toKeep);
            viewArr.push("T = Total Machines, C = Configuring, S3D = S3 Download, E = Executing, S3U = S3 Upload, F = Finished");
            viewArr.push('--------------------------------------------------------------------------------------');
            viewArr.push("Job ID : " + jobId);
            viewArr.push('--------------------------------------------------------------------------------------');
            if (data !== null && data.hasOwnProperty('FailedInstances')) {
                machinesFailed = data['FailedInstances']['count'];
                viewArr.push(machinesFailed + " machines failed to instantiate.");
            } else {
                machinesFailed = 0;
            }

            for (let key in data) {
                if (key == 'FailedInstances') {
                    continue;
                }
                viewArr.push('--------------------------------------------------------------------------------------');
                viewArr.push("Server Id : " + key);
                if (data[key].hasOwnProperty('masterStart')) {
                    configuring += 1;
                    var d = new Date(0);
                    d.setUTCSeconds(data[key]['masterStart']['time'] / 1000);
                    viewArr.push("Started at : " + d);
                    if (data[key].hasOwnProperty('configuration')) {
                        configuring -= 1;
                        s3downloading += 1;
                        viewArr.push("Configuration completed in " + (data[key]['configuration']['time'] - data[key]['masterStart']['time']) / 1000 + " seconds");
                        if ((data[key]['configuration']['time'] - data[key]['masterStart']['time']) / 1000 > maxOneOffTime) {
                            maxOneOffTime = (data[key]['configuration']['time'] - data[key]['masterStart']['time']) / 1000;
                        }
                        if (data[key].hasOwnProperty('s3Download')) {
                            s3downloading -= 1;
                            executing += 1;
                            viewArr.push("S3 download completed in " + (data[key]['s3Download']['time'] - data[key]['configuration']['time']) / 1000 + " seconds");

                            if ((data[key]['s3Download']['time'] - data[key]['configuration']['time']) / 1000 > maxdownloadTime) {
                                maxdownloadTime = (data[key]['s3Download']['time'] - data[key]['configuration']['time']) / 1000;
                            }

                            if (data[key].hasOwnProperty('partialFileExecution')) {

                                if (!recorded) {
                                    firstPartialTime = data[key]['partialFileExecution']['time'];
                                    recorded = true;
                                }
                                viewArr.push("File execution started. Time elapsed : " + (data[key]['partialFileExecution']['time'] - data[key]['s3Download']['time']) / 1000 + " seconds");
                                if (data[key].hasOwnProperty('s3Upload')) {
                                    executing -= 1;
                                    s3uploading += 1;
                                    viewArr.splice(-1, 1);
                                    viewArr.push("File Execution completed in " + (data[key]['partialFileExecution']['time'] - data[key]['s3Download']['time']) / 1000 + " seconds");
                                    if ((data[key]['partialFileExecution']['time'] - data[key]['s3Download']['time']) / 1000 > maxExecutionTime) {
                                        maxExecutionTime = (data[key]['partialFileExecution']['time'] - data[key]['s3Download']['time']) / 1000;
                                    }
                                    viewArr.push("S3 upload completed in " + (data[key]['s3Upload']['time'] - data[key]['partialFileExecution']['time']) / 1000 + " seconds");

                                    if ((data[key]['s3Upload']['time'] - data[key]['partialFileExecution']['time']) / 1000 > maxUploadTime) {
                                        maxUploadTime = (data[key]['s3Upload']['time'] - data[key]['partialFileExecution']['time']) / 1000;
                                    }
                                    if (data[key].hasOwnProperty('masterEnd')) {
                                        s3uploading -= 1;
                                        finished += 1;
                                        d = new Date(0);
                                        d.setUTCSeconds(data[key]['masterEnd']['time'] / 1000);
                                        viewArr.push("Terminated at : " + d);
                                        viewArr.push("Total time : " + (data[key]['masterEnd']['time'] - data[key]['masterStart']['time']) / 1000 + " seconds");
                                        viewArr.push("Baseline Completed");
                                    } else {
                                        viewArr.push("Terminating worker machine..");
                                        viewArr.push("--------------------------------------------------------------------------------------");
                                        continue;
                                    }
                                } else {
                                    viewArr.push("--------------------------------------------------------------------------------------");
                                    continue;
                                }
                            } else {
                                viewArr.push("File execution started. Time elapsed : " + diffInSecondsFromNow(data[key]['s3Download']['time']) + " seconds");
                                viewArr.push("--------------------------------------------------------------------------------------");
                                continue;
                            }
                        } else {
                            viewArr.push("s3 download started. Time elapsed : " + diffInSecondsFromNow(data[key]['configuration']['time']) + " seconds");
                            viewArr.push("--------------------------------------------------------------------------------------");
                            continue;
                        }
                    } else {
                        viewArr.push("Configuration started. Time elapsed : " + diffInSecondsFromNow(data[key]['masterStart']['time']) + " seconds");
                        viewArr.push("--------------------------------------------------------------------------------------");
                        continue;
                    }
                } else {
                    viewArr.push("Starting up your instance");
                    viewArr.push("--------------------------------------------------------------------------------------");
                    continue;
                }
                viewArr.push("--------------------------------------------------------------------------------------");
            }

            showView(viewArr);
            callback(false, true);
        };

        function isAllComplete(data, callback) {
            if (typeof(data) == "undefined" || data == null) {
                console.log("Stuck here");
                callback(false, false);
                return;
            }
            let aggregate = 0;
            if (machinesFailed > 0) {
                aggregate = machinesFailed - 1;
            }
            if (Object.keys(data).length == (vm.numOfMachines - aggregate)) {
                // if(true){
                for (let instance in data) {
                    if (!data[instance].hasOwnProperty('masterEnd')) {
                        if (instance == 'FailedInstances') {
                            continue;
                        }
                        callback(false, false);
                        return;
                    }
                }
                callback(false, true);
            } else {
                callback(false, false);
            }
        };


        vm.clearConsole = function () {
            viewArr = [];
            vm.message = "";
            viewArr.push("Welcome to NHANES AWS project!!");
            showView();
            return;
        }

        vm.recalculateCost = function () {
            let totalOneOff = maxOneOffTime;
            let oneOffCost = totalOneOff
            let totalDownloadTime = pessimisticDownloadTime * (numFiles / numMachines);
            let totalExecutionTime = pessimisticExecutionTime * (numFiles / numMachines);
            let totalUploadTime = pessimisticUploadTime * (numFiles / numMachines);
            let totalTime = totalDownloadTime + totalExecutionTime + totalUploadTime;
            vm.approxExecTime = Math.round(((totalOneOff + ((vm.numFiles / vm.numOfMachines) * totalTime)) / 60) * 1000) / 1000;
            let approxExecTimeHr = Math.round(((totalOneOff + ((vm.numFiles / vm.numOfMachines) * totalTime)) / 3600) * 1000) / 1000;
            let days = Math.round((approxExecTimeHr / 24) * 1000) / 1000;
            if (days > 1) {
                vm.approxExecTime = days + " days";
            } else if (vm.approxExecTime > 60) {
                vm.approxExecTime = approxExecTimeHr + " hours";
            } else {
                vm.approxExecTime = vm.approxExecTime + " minutes";
            }
            var price = IndexService.getPrice(vm.numOfMachines, vm.numFiles, maxOneOffTime, pessimisticDownloadTime, pessimisticExecutionTime, pessimisticUploadTime, vm.instanceType);
            vm.cost = Math.round(price * 10000) / 10000;
        };

        vm.setDatasetView = function (index) {
            vm.dataset = vm.dataSetList[index];
        };

        vm.setInstanceView = function (index) {
            vm.instanceType = vm.instanceList[index];
        };

        vm.uploadCodeFile = function () {
            upload(vm.codeFile);
        };

        vm.runBaseLine = function () {

            recorded = false;
            firstPartialTime = 0;
            maxOneOffTime = 0;
            maxdownloadTime = 0;
            pessimisticDownloadTime = 0;
            maxExecutionTime = 0;
            pessimisticExecutionTime = 0;
            maxUploadTime = 0;
            pessimisticUploadTime = 0;

            if ((url == "") || (vm.entrypoint == "") || (vm.numOfMachines == "") || (vm.instanceType == "None")) {
                vm.message += "<br>Required fields missing. Please make sure you fill all the information requested on the left.";
                updateScroll();
                return;
            }
            let body = {
                "entryPoint": vm.entrypoint,
                "numMachines": vm.numOfMachines,
                "algoPath": url,
                "dataset": vm.dataset,
                "numFiles": vm.numFiles,
                "perDSConcat": String(vm.perDSConcat),
                "compDSConcat": String(vm.compDSConcat),
                "instanceType": String(vm.instanceType),
            };

            IndexService.runBaseLine(body)
                .then(function (data) {
                    vm.s3Cost = data.data.s3Cost;
                    jobId = data.data.jobId;
                    vm.message += "<br>" + data.data.message;
                    vm.message += "<br>" + "Please wait till we fetch the running time and pricing estimates for your algorithm.";
                    vm.message += "<br>" + "Do not refresh or press the back button of your browser.";
                    vm.message += "<br>" + "Initializing " + vm.numOfMachines + " worker machine(s) for processing...";

                    viewArr.push(vm.message);

                    // let jobId = 'pcASpnFpqh';
                    let query = {
                        'jobId': jobId
                    };

                    socket = io.connect('http://34.215.122.196:3000/');
                    socket.emit('clientRequest', query);
                    socket.on('response', function (data) {
                        // socket.emit('end');
                        vm.jobList = data;
                        // $scope.$applyAsync(updateView(data, function(){return;}));
                        $scope.$applyAsync(updateView(data, jobId, function (err, res) {
                            if (err) {
                                console.log('Error in updating view');
                            } else {
                                isAllComplete(data, function (err, isComplete) {
                                    if (!isComplete) {
                                        setTimeout(function () {
                                            // socket = io.connect('http://34.211.238.12:3000/');
                                            vm.jobList = data;
                                            socket.emit('clientRequest', query);
                                        }, 5000);
                                        // setTimeout(function(){socket.emit('ack')}, 5000);
                                    } else {
                                        console.log(vm.compDSConcat)
                                        if(vm.compDSConcat){
                                            IndexService.concatenateAll(jobId)
                                                .then(function (data) {
                                                    vm.jobList = data;
                                                    socket.emit('end');
                                                    socket.disconnect();
                                                    vm.message += "Socket Disconnected..<br>Execution Complete.<br>";
                                                    pessimisticDownloadTime = maxdownloadTime / (numFiles / numMachines);
                                                    pessimisticExecutionTime = maxExecutionTime / (numFiles / numMachines);
                                                    pessimisticUploadTime = maxUploadTime / (numFiles / numMachines);
                                                    vm.message += "Metrics : <br>";
                                                    vm.message += "Maximum configuration time : " + maxOneOffTime + "<br>";
                                                    vm.message += "Maximum s3 download time : " + pessimisticDownloadTime + "<br>";
                                                    vm.message += "Maximum algorithm execution time : " + pessimisticExecutionTime + "<br>";
                                                    vm.message += "Maximum s3 upload time : " + pessimisticUploadTime + "<br>";
                                                    vm.message += "You can now tweak the values on the left based on the current baseline to estimate cost/run-time <br>";
                                                    vm.recalculateCost();
                                                    IndexService.uploadLogs(jobId, vm.message)
                                                })
                                        }
                                        else{
                                            vm.jobList = data;
                                            socket.emit('end');
                                            socket.disconnect();
                                            vm.message += "Socket Disconnected..<br>Execution Complete.<br>";
                                            pessimisticDownloadTime = maxdownloadTime / (numFiles / numMachines);
                                            pessimisticExecutionTime = maxExecutionTime / (numFiles / numMachines);
                                            pessimisticUploadTime = maxUploadTime / (numFiles / numMachines);
                                            vm.message += "Metrics : <br>";
                                            vm.message += "Maximum configuration time : " + maxOneOffTime + "<br>";
                                            vm.message += "Maximum s3 download time : " + pessimisticDownloadTime + "<br>";
                                            vm.message += "Maximum algorithm execution time : " + pessimisticExecutionTime + "<br>";
                                            vm.message += "Maximum s3 upload time : " + pessimisticUploadTime + "<br>";
                                            vm.message += "You can now tweak the values on the left based on the current baseline to estimate cost/run-time <br>";
                                            vm.recalculateCost();
                                            IndexService.uploadLogs(jobId, vm.message)
                                        }



                                    }
                                })
                            }
                        }));
                    });
                }, function (err) {
                    console.log(err.data);
                    vm.message += "<br>" + err.data;
                    updateScroll();
                })

        };

        vm.terminateExecution = function () {
            IndexService.terminateExecution(jobId)
                .then(function (data) {
                    vm.clearConsole();
                    viewArr.push("Job "+jobId+" Terminated");
                    updateScroll();

                })
        };

        function init() {
            IndexService
                .getInstanceTypeList()
                .then(function (data) {
                    console.log(data.data);
                    vm.instanceList = data.data;
                }, function (err) {
                    vm.message += "<br>Could not fetch Instance list. Please contact admin.";
                    updateScroll();
                });

            IndexService
                .getDatasetList()
                .then(function(list){
                    console.log(list.data);
                    vm.dataSetList = list.data;
                }, function(err){
                    vm.message += "<br> Could not fetch the data set list.";
                    updateScroll();
                });
        }


        function upload(file) {
            Upload.upload({
                url: '/codeUpload',
                data: {codeFile: file, entrypoint: vm.entrypoint}
            })
                .then(function (resp) {
                    url = resp.data;
                    vm.message += "<br>Upload Successful";
                    updateScroll();
                }, function (error) {
                    console.log(error);
                    vm.message += "<br>Upload failed. Please check if you have provided both the entrypoint and the zip file. If yes, please contact the administrator.";
                    updateScroll();
                })
        }

        init();
    }
})();
