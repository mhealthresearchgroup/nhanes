module.exports = function (data, masterJobList, callback){
	
	//Got from socket
	
	let jobId = data.jobId;
	let messageId = data.messageId;
	let time = data['time'];
	let step = data.step;
	//////////////////
	let message = "";
	
	if(masterJobList.hasOwnProperty(jobId)){
		//Job entry exists
		let currJob = masterJobList[jobId];
		if(step == "masterFail"){
			if(currJob.hasOwnProperty('FailedInstances')){
				masterJobList[jobId]['FailedInstances']['count'] += 1;
				callback(null, masterJobList);
				return;
			} else {
				masterJobList[jobId]['FailedInstances'] = {
					count : 1
				};
				callback(null, masterJobList);
				return;
			}
		}
		if(currJob.hasOwnProperty(messageId)){
			let currInstance = currJob[messageId];

			//instance entry exits
			switch (step) {
				case 'masterStart' :
					message = "Duplicate masterStart received";
					console.log(message);
					callback(message, null);
					return;

				case 'configuration' : 
					if(currInstance.hasOwnProperty(step)){
						message = "Errored..\n" + step + " NOT accounted in MJL.";
						console.log(message);
						callback(message, null);
						return;
					} else {
						currInstance[step] = {};
						currInstance[step]['time'] = time;
						currInstance[step]['count'] = 1;
						message = step + " accounted in MJL";
						console.log(message);
						callback(null, masterJobList);
						return;
					}

				case 's3Download' : 
					if(currInstance.hasOwnProperty(step)){
						message = "Errored..\n" + step + " NOT accounted in MJL.";
						console.log(message);
						callback(message, null);
						return;
					} else {
						currInstance[step] = {};
						currInstance[step]['time'] = time;
						currInstance[step]['count'] = 1;
						message = step + " accounted in MJL";
						console.log(message);
						callback(null, masterJobList);
						return;
					}

				case 'partialFileExecution' : 
					if(currInstance.hasOwnProperty(step)){
						currInstance[step]['time'] = time;
						currInstance[step]['count'] += 1;
						message = step + " accounted in MJL";
						console.log(message);
						callback(null, masterJobList);
						return;
					} else {
						currInstance[step] = {};
						currInstance[step]['time'] = time;
						currInstance[step]['count'] = 1;
						message = step + " accounted in MJL";
						console.log(message);
						callback(null, masterJobList);
						return;
					}

				case 's3Upload' :
					if(currInstance.hasOwnProperty(step)){
						message = "Errored..\n" + step + " NOT accounted in MJL.";
						console.log(message);
						callback(message, null);
						return;
					} else {
						currInstance[step] = {};
						currInstance[step]['time'] = time;
						currInstance[step]['count'] = 1;
						message = step + " accounted in MJL";
						console.log(message);
						callback(null, masterJobList);
						return;
					} 

				case  'masterEnd' :
					if(currInstance.hasOwnProperty(step)){
						message = "Errored..\n" + step + " NOT accounted in MJL.";
						console.log(message);
						callback(message, null);
						return;
					} else {
						currInstance[step] = {};
						currInstance[step]['time'] = time;
						currInstance[step]['count'] = 1;
						message = step + " accounted in MJL";
						console.log(message);
						callback(null, masterJobList);
						return;
					}
			}
		} else {
			//instance entry doesnt exist
			if(step == 'masterStart'){
				currJob[messageId] = {};
				currJob[messageId]['masterStart'] = {};
				currJob[messageId]['masterStart']['time'] = time;
				currJob[messageId]['masterStart']['count'] = 1;
				message = "New instance " + messageId + " accounted in MJL";
				console.log(message);
				callback(null, masterJobList);
				return;
			} else {
				message = "Errored..\n" + step + " NOT accounted in MJL.";
				console.log(message);
				callback(message, null);
				return;
			}
		}
	} else {
		//job entry doesnt exist
		//
		if(step == 'masterFail'){
			masterJobList[jobId] = {};
			masterJobList[jobId]['FailedInstances'] = {
				count : 1
			};
			message = "Instance creation failed";
			console.log(message);
			callback(null, masterJobList);
			return;
		}

		if(step == 'masterStart'){
			masterJobList[jobId] = {};
			masterJobList[jobId][messageId] = {
				masterStart : {
					'time' : time,
					'count' : 1
				}
			};
			message = "New job " + jobId + " accounted in MJL";
			console.log(message);
			callback(null, masterJobList);
			return;
		} else {
			message = "Errored..\n" + step + " NOT accounted in MJL.";
			console.log(message);
			callback(message, null);
			return;
		}
	}
};






















