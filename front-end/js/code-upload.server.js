'use strict';

const fs = require('fs');
const AWS = require('aws-sdk');
// AWS.config.loadFromPath(__dirname + "/../../awsConfig.json");
AWS.config.update({
    accessKeyId : process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey : process.env.AWS_SECRET_ACCESS_KEY,
    region : process.env.AWS_REGION
});
var s3 = new AWS.S3();
module.exports = function(req, res){
  if(typeof(req.file) == 'undefined' ||
    typeof(req.body) == 'undefined' ||
    typeof(req.body.entrypoint) == 'undefined' ||
    req.body.entrypoint == ''){
    console.log("Malformed request");
    res.status(400).send("Could not upload file");
    return;
  }
  let currPath = req.file.path + '.' + req.file.mimetype.split('/')[1];
  let metaArr;
  fs.rename(req.file.path, currPath, (err) => {
    if(err){
      console.log(err);
      res.status(400).send("Could not upload file. Rename failed.");
      return;
    } else {
      let uploadDetails = {
        originalName : req.file.originalname,
        uploadedOn : String(Date.now()),
        entrypoint : req.body.entrypoint
      };
      let bitmap = fs.readFileSync(__dirname + "/../" +currPath);
      let params = {
        Body : bitmap,
        Key : req.file.path.split('/')[1] + '.' + req.file.mimetype.split('/')[1],
        Bucket : 'neu-algorithm-code',
        // Prefix : 'algorithm',
        ServerSideEncryption: "AES256",
        Metadata : uploadDetails,
        ACL: 'public-read'
      };
      s3.putObject(params, function(err, data) {
       if (err) {
         fs.unlink(__dirname + "/../" +currPath, function(err){
          console.log(err);
          res.status(400).send("Could not upload file. S3 put object failed");
          return;
         })
       } // an error occurred
       else {
         console.log(data);           // successful response
         fs.unlink(__dirname + "/../" +currPath, function(err){
           if(err) {
             console.log("Could not delete file");
             let url = "https://s3.amazonaws.com/" + params.Bucket + "/" + params.Key;
             res.send(url);
           } else {
             let url = "https://s3.amazonaws.com/" + params.Bucket + "/" + params.Key;
             res.send(url);
           }
         })};
       });
     }
   })
 }
