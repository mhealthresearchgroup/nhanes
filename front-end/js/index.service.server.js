'use strict'
const AWS = require('aws-sdk');
// AWS.config.loadFromPath(__dirname + "/../../awsConfig.json");
AWS.config.loadFromPath(__dirname + "/../../awsConfig.json");
// AWS.config.update({
//     accessKeyId : process.env.AWS_ACCESS_KEY_ID,
//     secretAccessKey : process.env.AWS_SECRET_ACCESS_KEY,
//     region : process.env.AWS_REGION
// });
const S3 = new AWS.S3();
const request = require('request');
const uuidv1 = require('uuid/v1');
const sqs = new AWS.SQS({apiVersion: '2012-11-05'});
const randomString = require('randomstring');
const ec2 = new AWS.EC2();
let folderArr = [];


module.exports.uploadLogs = function (req, res) {
    console.log("Uploading logs")
    let jobId = req.body.jobId;
    let logs = req.body.logs;
    let params = {
        Body: JSON.stringify(logs),
        Bucket: "nhanes-data-output",
        Key: jobId+'/run_logs.html'
    };

    S3.putObject(params, function (err, data) {
    if (err) {
        console.log(err, err.stack);
        res.status(403).send("Error uploading log");
    } else {
        res.status(200).send("Log uploaded");
    }
});
}


module.exports.concatenateAll = function (req,res) {
    console.log("concatenating")
    let jobId = req.body.jobId;
    const ec2Params = {
        ImageId: 'ami-9fd601e7', // Ubuntu for R, Py and Nodejs
        InstanceType: 't2.nano',
        MaxCount: 1, /* required */
        MinCount: 1, /* required */
        TagSpecifications: [
            {
                ResourceType: 'instance',
                Tags: [
                    {
                        Key: 'Name',
                        Value: jobId+"_concatenate"
                    },
                    {
                        Key: 'createdBy',
                        Value: 'NEU'
                    }
                ]
            }],
        UserData: (new Buffer("#!/bin/bash \n " +
            "aws s3 cp s3://nhanes-data-processing-scripts/concatenate.zip /home/ubuntu/ \n" +
            "aws s3 cp s3://nhanes-data-output/" + jobId + "/ /home/ubuntu/temp/ --recursive\n" +
            "unzip /home/ubuntu/concatenate.zip -d /home/ubuntu/ \n" +
            "pip install -r /home/ubuntu/requirements.txt\n"+
            "python /home/ubuntu/concatenate.py /home/ubuntu/temp /home/ubuntu/" +jobId+".csv \n"+
            "aws s3 cp /home/ubuntu/" +jobId + ".csv s3://nhanes-data-output/"+jobId+"/\n"+
            "EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`\n" +
            "EC2_REGION=\"`echo \\\"$EC2_AVAIL_ZONE\" | sed -e 's:\\([0-9][0-9]*\\)[a-z]*\$:\\\\1:'`\" \n" +
            "aws ec2 terminate-instances --instance-ids $(curl -s http://169.254.169.254/latest/meta-data/instance-id) --region $EC2_REGION ").toString('base64')),
        KeyName: "ndacs-spades",
        IamInstanceProfile: {
            Arn: 'arn:aws:iam::893264666912:instance-profile/ndacs-app-role'
        },
        SecurityGroupIds: [
            'sg-18e84362'
        ],
        SubnetId: 'subnet-06f3cd4f'
    };
    ec2.runInstances(ec2Params, function (err, data) {
        if(err)
            res.status(500).send("Mega concatenation failed")
        else
            res.status(200).send("Mega concatenation successful")
    });
}

module.exports.terminateExecution = function (req, res) {
    let jobId = req.body.jobId;
    let params = {
        Filters: [
            {
                Name: 'tag:Name',
                Values: [jobId]
            }
        ]
    };
    ec2.describeInstances(params, function (err, data) {
        if(err)
            throw err;
        else{
            let instanceId = [];
            for(let index in data.Reservations){
                instanceId.push(data.Reservations[index].Instances[0].InstanceId)
            }
            var terminateParams = {
                InstanceIds: instanceId
            };
            ec2.terminateInstances(terminateParams, function (err, data) {
                if(err)
                    console.log(err);
                res.status(200).send("Execution terminated");
            })
        }
    })
}

module.exports.getDatasetList = function(req, res){
    let arr = ["nhanes-data-processing"];
    let params = {
        Bucket : 'nhanes-data-output',
        Delimiter : '/'
    };

    S3.listObjects(params, function(err, data){
        if(err) {
            res.status(500).send("Error in fetching buckets");
        } else {
            for(let obj in data.CommonPrefixes){
                let name = data.CommonPrefixes[obj].Prefix;

                //substr to remove the trailing '/' character
                arr.push(name.substr(0,name.length-1));
            }
            res.json(arr);
        }
    });
};

module.exports.getInstanceTypeList = function (req, res) {
    let arr = ["t2.nano", "t2.micro", "t2.small", "m4.large"];
    res.json(arr);
};

module.exports.runBaseLine = function(req, res){

    let algo_entry_point_filename = req.body.entryPoint;
    let temp = req.body.algoPath.split('/');
    let algo_path = "s3://"+temp[temp.length-2]+"/"+temp[temp.length-1];
    let numFiles = parseInt(req.body.numFiles);
    let numMachines = req.body.numMachines;
    let totalFiles  = 306;

    let s3params = {
        Bucket : "nhanes-data-input-2",
        Delimiter : "/"
    };
    let batch = [];
    S3.listObjectsV2(s3params, function(err, data){
        if(err){
            console.log(err);
            res.status(403).send("Bucket Not Accessible");
            return;
        }
        // console.log(data);

        let randStr = randomString.generate({
            length : 10,
            charset: 'alphabetic',
        });

        let index=0;
        let fileList = [];
        let folders = [];

        let batchSize = Math.floor(numFiles/numMachines);
        let sqsparams = {
            MessageBody: "Details of algorithm to be executed along with file lists in s3",
            QueueUrl:"https://sqs.us-west-2.amazonaws.com/893264666912/pam-unprocessed-files.fifo",
            MessageGroupId: uuidv1()
        };

        let dat = data.CommonPrefixes;
        let count =0;
        // let anomaly = ["p4uFJ10308", "x6K9M10317", "PmxH812376", "oSMG115595", "uWrdl16830", "DBPzE16262"]

        // let foldersToUse =  [ '5H0Ap32607M50',
        //     '8Zr5f27863F71',
        //     'BgmFr49930M17',
        //     'CUa9a31806F51',
        //     'FkFNJ84381M75',
        //     'HTwp246977F65',
        //     'JxmPO43705F28',
        //     'NB8R646606F14',
        //     'O1FUR71518M6',
        //     'REf2G78189M62',
        //     'TntON72772F19',
        //     'Y1c9e32280F30',
        //     'ZNH6C40453F67',
        //     'bzIZn77956M40',
        //     'ctJsp13856F58',
        //     'e9LYu27901M43',
        //     'gC9eT63305F37',
        //     'gkqDA81142M74',
        //     'guFd423359M80',
        //     'jmIxH19183F21',
        //     'l0VyU19504M24',
        //     'nA6LU67495F56',
        //     'pqvjM58349M4',
        //     'rJFQy76470F9',
        //     'u4DXN94827M35',
        //     'zS05G81766M47' ]

        for (let item in dat) {
            let file = dat[item]['Prefix'].split('/')[0];
            // if (file !== '' && foldersToUse.indexOf(file) > -1){
            if (file !== '') {
                fileList.push('s3://' + s3params.Bucket + '/' + file+'/');

                folders.push(file);
            }
        }
        if(numFiles<totalFiles) {
            if(numFiles<folders.length) {
                fileList = fileList.splice(0,numFiles);
            }
        } else {
            let temp = folders.length;
            while (temp < numFiles) {
                for (let index in fileList) {
                    fileList.push(fileList[index]);
                    temp = fileList.length;
                    if (temp == numFiles) {
                        break;
                    }
                }
            }
        }


        //
        /*
         New code for batching (bug fix)
         */
        let temp = fileList.splice(0,numFiles);
        for(let i =0; i<numMachines;i++){
            batch[i] = temp.splice(0,1);
        }
        while (temp.length!==0) {
            for(let i =0; i<numMachines;i++){
                if(temp.length===0)
                    break;
                else{
                    batch[i].push(temp.splice(0,1))
                }
            }
        }

        /*
         End of new code
         */

        let successList= [];
        let errorList = [];
        let counts = 0;
        let isErrored = false;
        for (let i = 0; i < batch.length; i++) {
            // Introduce a pause after pushing 10 messages to queue
            if (i % 10 == 0)
                for (let x = 0; x < 10000; x++);
            sqsparams.MessageAttributes = {
                "JobId" :{
                    DataType : "String",
                    StringValue: randStr
                },
                "EntryPoint": {
                    DataType: "String",
                    StringValue: algo_entry_point_filename
                },
                "Files": {
                    DataType: "String",
                    StringValue: batch[i].toString()
                },
                "AlgorithmPath":{
                    DataType: "String",
                    StringValue: algo_path
                },
                "invokerIPAddress" : {
                    DataType: "String",
                    // StringValue: "http://127.0.0.1:3000"
                    StringValue: "http://34.215.122.196:3000"
                },
                "invokerPrivateAddress" : {
                    DataType : "String",
                    // StringValue: "http://127.0.0.1:3000"
                    StringValue : "http://172.31.1.198:3000"
                },
                "PerDSConcat" : {
                    DataType: "String",
                    StringValue : req.body.perDSConcat
                },
                "CompleteDSConcat" : {
                    DataType: "String",
                    StringValue : req.body.compDSConcat
                },
                "instanceType" : {
                    DataType: "String",
                    StringValue : req.body.instanceType
                }
            };
            sqsparams.MessageDeduplicationId = uuidv1();

            sqs.sendMessage(sqsparams, function (err, data) {
                if (isErrored || (counts === batch.length - 1)) {
                    if (counts == batch.length - 1) {
                        let s3 = {
                            Bucket : "nhanes-data-input",
                        };

                        let total = 0;
                        Promise
                            .all(folderArr.map((folder)=>{
                                return new Promise(resolve => {
                                    s3.Prefix = folder.toString();
                                    S3.listObjectsV2(s3, function (err, data) {
                                        resolve([folder, data.KeyCount])
                                    })
                                });
                            }))
                            .then(respo => {
                                total = 0
                                for (var i = respo.length - 1; i >= 0; i--) {
                                    total += parseInt(respo[i][1])
                                }
                                let get =  (total/10000)*.004;
                                let put = (total/1000)*.005;
                                let s3Cost = get + put;
                                console.log("S3 usage cost:", s3Cost);
                                res.json({
                                    jobId : randStr,
                                    message : "Messages pushed to the queue successfully",
                                    s3Cost : s3Cost
                                });
                            })
                            .catch(err => {
                                console.log(err);
                                res.sendStatus(500)
                            })
                    }
                    return;
                } else {
                    if(err){
                        isErrored = true;
                        res.json({
                            jobId : randStr,
                            message : batch.length-counts+" Messages failed"
                        });
                    } else {
                        counts ++;
                    }
                }
            });
        }
    });
};