/**
 * Created by sushantmimani on 9/22/17.
 */

'use strict';

const aws = require('aws-sdk');
aws.config.loadFromPath(__dirname + "/../../awsConfig.json");
// aws.config.update({
//     accessKeyId : process.env.AWS_ACCESS_KEY_ID,
//     secretAccessKey : process.env.AWS_SECRET_ACCESS_KEY,
//     region : process.env.AWS_REGION
// });
const uuidv1 = require('uuid/v1');
const s3 = new aws.S3();
const sqs = new aws.SQS({apiVersion: '2012-11-05'});


module.exports.enqueue = function (req, res) {

    console.log(req.body);
    return;
    let algo_entry_point_filename = req.body.entryPoint;
    let algo_path = req.body.algoPath;
    let numFiles = req.body.numFiles;
    let numMachines = req.body.numMachines;

    // const s3params = {
    //     Bucket: 'pam-data',
    //     Prefix: '08-22-2017'
    // };

    let s3params = {
        Bucket: 'nhanes-data-processing',
        Prefix: 'input/test/99858/'
    };


    s3.listObjectsV2(s3params, function (err, data) {
        if (err) {
            console.log(("Error while fetching from S3"))
            res.status(400).send(err); // an error occurred

        }
        else {
            let index=0;
            let fileList = [];
            let batch = [];
            let batchSize = Math.floor(numFiles/numMachines);
            let sqsparams = {
                MessageBody: "Details of algorithm to be executed along with file lists in s3",
                QueueUrl:"https://sqs.us-west-2.amazonaws.com/893264666912/pam-unprocessed-files.fifo",
                MessageGroupId: uuidv1()
            };
            let dat = data.Contents;
            for (let item in dat) {
                let file = dat[item]['Key'].split('/')[1];
                if (file !== '')
                    fileList.push('s3://' + s3params.Bucket + '/' + dat[item]['Key'])
            }

            // Returns: Array of Array
            while(index<numMachines-1){
                // Push an array of batchSize into batch array
                batch.push(fileList.splice(0, batchSize))
                index++
            }
            if(numFiles>index*batchSize)
                batch.push(fileList.splice(0,numFiles-index*batchSize));

            let successList= [];
            let errorList = []
            for (let i = 0; i < batch.length; i++) {
                console.log("Sending request to queue", i);
                sqsparams.MessageAttributes = {
                    "EntryPoint": {
                        DataType: "String",
                        StringValue: algo_entry_point_filename
                    },
                    "Files": {
                        DataType: "String",
                        StringValue: batch[i].toString()
                    },
                    "AlgorithmPath":{
                        DataType: "String",
                        StringValue: algo_path

                    }
                };


                sqsparams.MessageDeduplicationId = uuidv1();

                //TODO Uncomment this for messages to be pushed in the queue..
                // sqs.sendMessage(sqsparams, function (err, data) {
                //     if (err) {
                //         console.log("Error is", err);
                //     } // an error occurred
                //     else {
                //         console.log(data);
                //     }

                // });
            
            }


            res.status(200).send("Messages queued");
        }
    })

}