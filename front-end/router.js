'use strict';
let codeUpload = require('./js/code-upload.server.js');
const multer = require('multer');
const IndexService = require('./js/index.service.server.js');
const Enqueue = require('./js/enqueue.server');
const upload = multer({ dest: 'temp/' });



module.exports = function(app) {
    app.post('/codeUpload', upload.single('codeFile'), codeUpload);
    app.get('/dataSetList', IndexService.getDatasetList);
    app.get('/instanceTypeList', IndexService.getInstanceTypeList);
    app.post('/runBaseLine', IndexService.runBaseLine);
    app.post('/enqueue', Enqueue.enqueue);
    app.post('/terminateExecution', IndexService.terminateExecution);
    app.post('/uploadLogs', IndexService.uploadLogs);
    app.post('/concatenateAll', IndexService.concatenateAll);
}
