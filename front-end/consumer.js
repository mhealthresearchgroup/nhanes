/**
 * Created by sushantmimani on 9/10/17.
 */

'use strict'
// import Consumer from "sqs-consumer";
// import {execFile} from "child_process";

let Consumer = require('sqs-consumer');
let {execFile}= require('child_process');
// const io = require('socket.io-client');

//Change this to address of the web server
//so that both consumer and worker machines are able to send the updates

let jobCompletionScriptPath = "s3://nhanes-job-completion-scripts/v1";
console.log("Consumer running");
const app = Consumer.create({

    queueUrl:"https://sqs.us-west-2.amazonaws.com/893264666912/pam-unprocessed-files.fifo",
    waitTimeSeconds:1,
    messageAttributeNames: ['EntryPoint','Files','AlgorithmPath','invokerIPAddress', 'JobId', 'invokerPrivateAddress', 'PerDSConcat', 'CompleteDSConcat', 'instanceType'],
    handleMessage: (message, done) => {
        let customInstanceId = message.MessageId;
        const algo_entry_point_filename = message.MessageAttributes['EntryPoint']['StringValue'];
        const filePathList = message.MessageAttributes['Files']['StringValue'];
        const algo_path = message.MessageAttributes['AlgorithmPath']['StringValue'];
        const invokerIPAddress = message.MessageAttributes['invokerIPAddress']['StringValue'];
        const invokerPrivateAddress = message.MessageAttributes['invokerPrivateAddress']['StringValue'];
        const jobId = message.MessageAttributes['JobId']['StringValue'];
        const concat_files = message.MessageAttributes['PerDSConcat']['StringValue'];
        const files = filePathList.split(',');
        const perDSConcat = message.MessageAttributes['PerDSConcat']['StringValue'];
        const completeDSConcat = message.MessageAttributes['CompleteDSConcat']['StringValue'];
        const instanceType = message.MessageAttributes['instanceType']['StringValue'];


        const child = execFile('node', ['launchEC2', algo_entry_point_filename, filePathList, algo_path,
            customInstanceId, jobCompletionScriptPath, invokerIPAddress, invokerPrivateAddress, jobId, concat_files, instanceType],

            (error, stdout, stderr) => {
            if (error) {
                throw error;
            }
            else if (stderr) {
                console.log(stderr);
            }
            else {
                // let obj = {
                //     jobId : jobId,
                //     messageId : customInstanceId,
                //     time : (new Date).getTime(),
                //     step: "masterStart"
                // };
                // socket.emit('event', obj);
                // socket.on('ack', function(){
                //     socket.emit('end');
                //     socket.disconnect();
                // })
                
                console.log(stdout);
            }
        });
        done();
    }
});

app.on('error', (err) => {
    console.log(err.message);
});

app.start();