let express = require('express');
let bodyParser = require('body-parser');
let socket = require('socket.io');
let app = express();
let masterJobList = {};
let eventHandler = require('./js/index.server.socket.handler.js');
app.use(bodyParser.json({type : 'application/json'}));
app.use(express.static(__dirname + '/public'));
require('./router')(app);
require('./newConsumer');
let listener = app.listen(3000, () => (console.log("Listening on port 3000")));
let io = socket(listener);


io.on('connection', function(socket){
	console.log("Connected");
	socket.on('event', function(data){
		// console.log(socket);
		console.log(data);
		eventHandler(data, masterJobList, function(err, updatedMasterJobList){
			if(err){
				console.log("Errored...", err);
				socket.emit('ack');
			} else {
				masterJobList = updatedMasterJobList;
				console.log("The new list is");
				console.log(JSON.stringify(masterJobList));
				socket.emit('ack');
			}
		});
	});
	socket.on('clientRequest', function(data){
		//fill in client request here
		console.log("Client request received");
		let jobId = data.jobId;
		socket.emit('response', masterJobList[jobId]);
		socket.on('ack', function(){
			socket.emit('response', masterJobList[jobId]);
		});
	});
	socket.on('end', function(){
		console.log("Here");
		socket.disconnect();
	});
});