/**
 * Created by sushantmimani on 10/23/17.
 */

'use strict'

let AWS = require('aws-sdk');
AWS.config.loadFromPath(__dirname + "/../awsConfig.json");
var sqs = new AWS.SQS();
const uuidv1 = require('uuid/v1');
let {execFile} = require('child_process');
let jobCompletionScriptPath = "s3://nhanes-job-completion-scripts/v1";
let launchEC2 = require('./launchEC2');

var params = {
    QueueUrl: 'https://sqs.us-west-2.amazonaws.com/893264666912/pam-unprocessed-files.fifo',
    MaxNumberOfMessages: 5,
    MessageAttributeNames: ['EntryPoint', 'Files', 'AlgorithmPath', 'invokerIPAddress', 'JobId',
        'invokerPrivateAddress', 'PerDSConcat', 'CompleteDSConcat', 'instanceType', 'MessageDeduplicationId', 'MessageGroupId']
    // WaitTimeSeconds: 1
};

setInterval(consume, 5)

function consume() {

    sqs.receiveMessage(params, function (err, data) {
        if (err) {
            console.log(err, err.stack);
        } else {

            if (data.hasOwnProperty("Messages")) {
                for (let index in data.Messages) {
                    const message = data.Messages[index];

                    launchEC2(message, function (err, data) {
                        if (err) {
                            console.log("Printing error in consumer", error);
                            console.log("Reposting to queue")
                            let sendParams = {
                                MessageBody: message.Body,
                                QueueUrl: params.QueueUrl,
                                MessageDeduplicationId: uuidv1(),
                                MessageGroupId: uuidv1(),
                            };
                            sendParams.MessageAttributes = {
                                "JobId": {
                                    DataType: "String",
                                    StringValue: jobId
                                },
                                "EntryPoint": {
                                    DataType: "String",
                                    StringValue: algo_entry_point_filename
                                },
                                "Files": {
                                    DataType: "String",
                                    StringValue: filePathList
                                },
                                "AlgorithmPath": {
                                    DataType: "String",
                                    StringValue: algo_path
                                },
                                "invokerIPAddress": {
                                    DataType: "String",
                                    // StringValue: "http://127.0.0.1:3000"
                                    StringValue: "http://34.215.122.196:3000"
                                },
                                "invokerPrivateAddress": {
                                    DataType: "String",
                                    // StringValue: "http://127.0.0.1:3000"
                                    StringValue: "http://172.31.1.198:3000"
                                },
                                "PerDSConcat": {
                                    DataType: "String",
                                    StringValue: perDSConcat
                                },
                                "CompleteDSConcat": {
                                    DataType: "String",
                                    StringValue: completeDSConcat
                                },
                                "instanceType": {
                                    DataType: "String",
                                    StringValue: instanceType
                                }
                            };
                            sqs.sendMessage(sendParams, function (err, data) {
                                if (err) {
                                    console.log("Error in reposting", err);
                                }
                                else {
                                    console.log("Message reposted successfully");
                                    console.log(data);
                                }
                            });
                        }

                        else {
                            console.log(data);
                        }
                        var delParams = {
                            QueueUrl: params.QueueUrl, /* required */
                            ReceiptHandle: message.ReceiptHandle /* required */
                        };
                        sqs.deleteMessage(delParams, function (err, data) {
                            if (err)
                                console.log("Message not deleted");
                            else
                                console.log("Message deleted");

                        });


                    });
                }

            }
        }

    });
}


