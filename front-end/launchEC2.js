/**
 * Created by sushantmimani on 9/13/17.
 */

'use strict'


let aws = require('aws-sdk');
aws.config.loadFromPath(__dirname + "/../awsConfig.json");
const io = require('socket.io-client');
const ec2 = new aws.EC2();

module.exports = function (message,callback) {
    const algo_entry_point_filename = message.MessageAttributes['EntryPoint']['StringValue'];
    let filePathList = message.MessageAttributes['Files']['StringValue'].split(",")
    const algo_path = message.MessageAttributes['AlgorithmPath']['StringValue'];
    let algo_type = algo_entry_point_filename.split(".")[1];
    const invokerIPAddress = message.MessageAttributes['invokerIPAddress']['StringValue'];
    const invokerPrivateAddress = message.MessageAttributes['invokerPrivateAddress']['StringValue'];
    const jobId = message.MessageAttributes['JobId']['StringValue'];
    const perDSconcat = message.MessageAttributes['PerDSConcat']['StringValue'];
    const instance_type = message.MessageAttributes['instanceType']['StringValue'];
    const jobCompletionScriptPath = "s3://nhanes-job-completion-scripts/v1";
    let algo_filename = algo_path.split("/").slice(-1)[0];
    let userPath = "/home/ubuntu/";
    let socket = io.connect(invokerIPAddress);
    switch (algo_type) {
        case 'py':
            algo_type = 'python';
            break;
        case 'R':
            algo_type = 'R';
            break;
        default:
            algo_type = 'java';
    }

    filePathList = filePathList.map(filePath => {
        let temp = filePath.split('/');
        let folder = temp[temp.length-2];
        return "mkdir -p /home/ubuntu/input/"+folder.slice(5)+"\nmkdir -p /home/ubuntu/output/"+folder.slice(5)+
            "\naws s3 cp " + filePath + " /home/ubuntu/input/"+folder.slice(5)+"/ --recursive  --exclude \"*\"  --include \"GT3XPLUS-AccelerationCalibrated*\" \n";//+
    });

    const params = {
        ImageId: 'ami-9fd601e7', // Ubuntu for R, Py and Nodejs
        InstanceType: instance_type,
        MaxCount: 1, /* required */
        MinCount: 1, /* required */
        TagSpecifications: [
            {
                ResourceType: 'instance',
                Tags: [
                    {
                        Key: 'Name',
                        Value: jobId
                    },
                    {
                        Key: 'createdBy',
                        Value: 'NEU'
                    }
                    /* more items */
                ]
            }],
        UserData: (new Buffer("#!/bin/bash \n " +
            "aws s3 cp s3://nhanes-data-processing-scripts/env.sh " + userPath + "\n" +
            "source " + userPath + "env.sh \n" +
            "aws s3 cp " + jobCompletionScriptPath + " " + userPath + " --recursive \n" +
            "aws s3 cp s3://nhanes-data-processing-scripts/bash-Ubuntu.sh " + userPath + "\n" +
            "aws s3 cp s3://nhanes-data-processing-scripts/config.sh " + userPath + "\n" +
            "chmod +x " + userPath + "bash-Ubuntu.sh\n" +
            "chmod +x " + userPath + "config.sh\n" +
            "node " + userPath + "job_completion.js $INSTANCEID " + " " + invokerPrivateAddress + " configuration " + jobId + "\n" +
            filePathList.join('') +
            "aws s3 cp " + algo_path + " " + userPath + "\n" +
            "node " + userPath + "job_completion.js $INSTANCEID " + invokerPrivateAddress + " s3Download " + jobId + "\n" +
            "unzip " + userPath + algo_filename + " -d " + userPath + "\n" +
            "." + userPath + "config.sh " + algo_type + "\n" +
            "ls " + userPath + "input > " + userPath + "list.txt \n" +
            "." + userPath + "bash-Ubuntu.sh  " + userPath+algo_entry_point_filename + " " + algo_type + " " + userPath + "input " + userPath + "output " + userPath + "logs " + userPath + "list.txt  $INSTANCEID " + invokerPrivateAddress + " " + jobId + " " + perDSconcat + "\n" +
            "aws s3 cp " + userPath + "output s3://nhanes-data-output/" + jobId + "/  --recursive\n" +
            "node " + userPath + "job_completion.js $INSTANCEID " + invokerPrivateAddress + " s3Upload " + jobId + "\n" +
            "EC2_AVAIL_ZONE=`curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone`\n" +
            "EC2_REGION=\"`echo \\\"$EC2_AVAIL_ZONE\" | sed -e 's:\\([0-9][0-9]*\\)[a-z]*\$:\\\\1:'`\" \n" +
            "node " + userPath + "job_completion.js $INSTANCEID " + invokerPrivateAddress + " masterEnd " + jobId + "\n" +
            "aws ec2 terminate-instances --instance-ids $(curl -s http://169.254.169.254/latest/meta-data/instance-id) --region $EC2_REGION ").toString('base64')),
        KeyName: "ndacs-spades",
        IamInstanceProfile: {
            Arn: 'arn:aws:iam::893264666912:instance-profile/ndacs-app-role'
        },
        SecurityGroupIds: [
            'sg-18e84362'
        ],
        SubnetId: 'subnet-06f3cd4f'
    };

    ec2.runInstances(params, function (err, data) {
        if (err) {
            let obj = {
                jobId: jobId,
                messageId: null,
                message: err.message,
                time: (new Date).getTime(),
                step: "masterFail"
            };
            socket.emit('event', obj);
            socket.on('ack', function () {
                socket.emit('end');
                socket.disconnect();
            })
            callback(err)
        } else {
            let obj = {
                jobId: jobId,
                messageId: data.Instances[0].InstanceId,
                time: (new Date).getTime(),
                step: "masterStart"
            };
            socket.emit('event', obj);
            socket.on('ack', function () {
                socket.emit('end');
                socket.disconnect();
            })
            callback(false, "Instance started "+ data.Instances[0].InstanceId)
        }
    });

}

