'use strict'
const io = require('socket.io-client');
// const instanceId = process.env.customInstanceId;
const instanceId = process.argv[2];
// const destinationIP = process.env.invokerIPAddress;
const destinationIP = process.argv[3];
const jobId = process.argv[5];

let socket = io.connect(destinationIP);
let obj = { 
	jobId : jobId,
	messageId : instanceId,
	time : (new Date).getTime(),
    step: process.argv[4]
	};
socket.emit('event', obj);
socket.on('ack', function(){
	socket.emit('end');
	socket.disconnect();
	process.exit();
});

