import os,sys
import pandas as pd
from datetime import datetime

def mhealth_timestamp_parser(str):
    MHEALTH_TIMESTAMP_FORMAT = "%Y-%m-%d %H:%M:%S.%f"
    return datetime.strptime(str, MHEALTH_TIMESTAMP_FORMAT)


def getISM(df):
    items = df.iloc[:,1].values.tolist()
    if all(x == items[0] for x in items):
        return 1
    else:
        return 0


def main(argv):
    df = pd.read_csv(argv[0], header=0, sep=',', compression="infer", quotechar='"', parse_dates=[0],
                     date_parser=mhealth_timestamp_parser)
    start = df['HEADER_TIMESTAMP'].iloc[0]
    stop = df['HEADER_TIMESTAMP'].iloc[-1]
    dur = (stop - start).seconds
    time_grouper = pd.TimeGrouper(key='HEADER_TIMESTAMP', freq='10S')
    grouped_df = df.groupby(time_grouper)
    mean_df = grouped_df.agg(lambda x: getISM(x))
    mean_df = mean_df.reset_index(level=['HEADER_TIMESTAMP'])
    mean_df = mean_df.ix[:, 0:2]
    mean_df.columns = ['HEADER_TIMESTAMP', 'ISMOn']
    outFileName = os.path.basename(argv[0])[0:-14] + ".ISMO.csv"
    outSuf = argv[1]
    outPath = os.path.join(outSuf, outFileName)
    mean_df.to_csv(outPath, index=False)


if __name__ == '__main__':
    main(sys.argv[1:])









