/**
 * Created by sushantmimani on 9/10/17.
 */

import {exec} from "child_process";
import Consumer from "sqs-consumer";

const app = Consumer.create({

    queueUrl: 'https://sqs.us-west-2.amazonaws.com/551420442133/nhanes-test.fifo',
    messageAttributeNames: ['Algorithm','Files'],
    handleMessage: (message, done) => {
        let numFiles,
            algo,
            index = 0,
            files = ['file-1.txt','file-2.txt','file-3.txt','file-4.txt','file-5.txt'];
        if (message.MessageAttributes.hasOwnProperty('Algorithm')) {
            algo = message.MessageAttributes['Algorithm']['StringValue'];
        }
        if (message.MessageAttributes.hasOwnProperty('Files')) {
            numFiles = parseInt(message.MessageAttributes['Files']['StringValue']);
        }
        while(numFiles>0){
            console.log("Calling subprocess");
            numFiles--;
            let imageName = files[index].split('.')[0];
            exec('./script.sh '+algo+' '+files[index++]+' '+imageName, (error, stdout, stderr) => {
                if (error) {
                    console.error(`exec error: ${error}`);
                    return;
                }
                else if (stderr) {
                    console.error(`algo error: ${stderr}`);
                    return;
                }
                console.log("stdout: ", stdout)
            });

        }
        done();
    }
});

app.on('error', (err) => {
    console.log(err.message);
});

app.start();