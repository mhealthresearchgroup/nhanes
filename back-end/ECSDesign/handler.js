'use strict';

let aws = require('aws-sdk');
let uuidv1 = require('uuid/v1');
const s3 = new aws.S3();

module.exports.request= (event, context, callback) => {
    let body = JSON.parse(event.body);
    let algo_entry_point_filename = body.entryPoint;
    let algo_path = body.algoPath
    let numFiles = body.numFiles;
    let numMachines = body.numMachines;
    let sqs = new aws.SQS({apiVersion: '2012-11-05'});
    let s3 = new aws.S3();


    let s3params = {
        Bucket: 'pam-data',
        Prefix: '08-22-2017'
    };


    const response = {
        statusCode: 200,
        body: JSON.stringify({
            message: 'Requests queued successfully'

        }),
    };

    callback(null,response)
}

