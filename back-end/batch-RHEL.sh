#!/bin/bash

while read line ; do
    dirname=$(echo $line | cut -f1 -d'.')
    mkdir -p $dirname
	echo "FETCHING FILE $line FROM S3"
    aws s3api get-object --bucket pam-data --key 08-22-2017/$line $line
    java -jar GT3XParser.jar $line $dirname G_VALUE WITH_TIMESTAMP SPLIT MHEALTH SUMMARY_ON DEBUG_ON
    echo 'GZIPPING INDIVIDUAL FILES'
    gzip $dirname/*.csv
    echo 'UPLOADING TO S3'
    aws s3 mv $dirname s3://nhanes-data-processing/batch/$dirname --recursive
    echo 'CLEANING UP'
    rm -rf $line
    rm -rf $dirname
done < list.txt

# ls -d  */ | xargs rm -rf
# ./batch 2>&1 | tee batch.log
# ./batch &> batch.log &
# awk 'NR>37' list.txt
# aws s3 ls s3://pam-data/08-22-2017/ | wc -l 
# Inside single quotes, everything is characters



# aws s3 ls s3://pam-data/08-22-2017/ | tr -s ' ' | cut -f4 -d' ' | grep gt3x | awk 'NR>100 && NR<=150' > list.txt
# wget https://github.com/SPADES-PUBLIC/mHealth-GT3X-converter-public/archive/master.zip && unzip master.zip && mv mHealth-GT3X-converter-public-master/GT3XParser.jar .
# touch batch.sh && chmod +x batch.sh && vim batch.sh

# ls /output/17047 |  wc -l
# ls logs/ | wc -l

# ls data > ls.txt
# while read dirname; do
#     find ${dirname} -name *.csv -print0  | xargs -0 -I file cat file | grep -v "HEADER_TIME_STAMP,SMART_COUNTS" >> out/${dirname}.csv 
# do < ls.txt

#  find . -name "*.csv" | xargs wc -l 
#  while read dirname; do ls $dirname | wc -l; done < ls.txt | grep -v 193
