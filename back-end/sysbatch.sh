#!/bin/bash

ALGO=$1
ALGO_TYPE=$2
IN_DIR=$3
OUT_DIR=$4
LOG_DIR=$5
SPLIT_SIZE=$6


echo "Splitting $TOTAL files into $SPLIT_SIZE each"
ls $IN_DIR > list.txt
TOTAL=$(wc -l list.txt | cut -f1 -d' ')
touch megalist.txt && > megalist.txt
for (( i=0; i<=$TOTAL; i+=$SPLIT_SIZE )); do
	let limit=$i+$SPLIT_SIZE
	filename=list_${i}_${limit}
	echo "Saving files from $i to $limit in $filename"
	awk "NR>${i} && NR<=${limit}" list.txt > $filename
	echo $filename >> megalist.txt
done


echo "Starting Algorithm in the background"
while read filename; do 
    bash ./batch.sh $ALGO $ALGO_TYPE $IN_DIR $OUT_DIR $LOG_DIR $filename &
done < megalist.txt