#!/bin/bash

ALGO=$1
ALGO_TYPE=$2
IN_LOCAL=$3
OUT_LOCAL=$4
LOG_DIR=$5
FILES_PATH=$6
CUSTOM_INSTANCE_ID=$7
INVOKER_IP_ADDRESS=$8
JOB_ID=$9
CONCAT=${10}


while read dirname; do
    echo "Processing files for "$dirname
    ls $IN_LOCAL/$dirname  > /home/ubuntu/files.txt
    while read filepath; do
        echo "GUNZIP "$IN_LOCAL/$dirname/$filepath
        gunzip $IN_LOCAL/$dirname/$filepath
    done < /home/ubuntu/files.txt
    if [ "$CONCAT" = true ]; then
        echo "CONCAT "$IN_LOCAL/$dirname/$filepath
        find $IN_LOCAL/$dirname -name *.csv -print0 | sort -z | xargs -0 -I file cat file >> $OUT_LOCAL/${dirname}-tmp.csv
        grep -v 'HEADER_TIMESTAMP,ACTIVITY_COUNT' $OUT_LOCAL/${dirname}-tmp.csv > $OUT_LOCAL/${dirname}.csv
        sed -i '1i HEADER_TIMESTAMP,ACTIVITY_COUNT' $OUT_LOCAL/${dirname}.csv
        rm -f $OUT_LOCAL/${dirname}-tmp.csv
        rm -rf $IN_LOCAL/$dirname
    fi
done < $FILES_PATH
