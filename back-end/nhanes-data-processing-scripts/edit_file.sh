#!/usr/bin/env bash

DIRNAME=$1

ls  $DIRNAME | while read filename; do
    sed '/HEADER_TIMESTAMP/ d' $DIRNAME/$filename > $DIRNAME/temp
    rm $DIRNAME/$filename
    mv $DIRNAME/temp $DIRNAME/$filename
done