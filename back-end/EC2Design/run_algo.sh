#!/bin/bash

# INPUT ARGS: Algorithm file
$ALGO=$1
$ALGO_TYPE=$2
unzip $ALGO
case $ALGO_TYPE in
    'python' )  pip install -r requirements.txt 
                ;;
    'R' )   yum install -y R
            R -e "install.packages('shiny', repos='http://cran.rstudio.com/')"
            wget https://download3.rstudio.org/centos5.9/x86_64/shiny-server-1.4.2.786-rh5-x86_64.rpm
            yum install -y --nogpgcheck shiny-server-1.4.2.786-rh5-x86_64.rpm 
            ;;
esac

$IN_BUCKET='nhanes-data-processing'
$OUT_BUCKET='out-bucket'
$OUT_LOCAL='output'

while read dirpath ; do
    dirname=$(echo $dirpath | awk -F"//" '{$0=$2}1' | cut -f3 -d'/')
    mkdir -p $dirname
    mkdir -p $OUT_LOCAL
	echo "FETCHING ALL FILES IN $dirname"
    aws s3 cp $dirpath $OUT_LOCAL
    while read filepath; do
        case $ALGO_TYPE in
            'python' )  python $ALGO/$ALGO $filepath $OUT_LOCAL &> $filepath.log
                        ;;
            'R' ) R < $ALGO/$ALGO $filepath $OUT_LOCAL --no-save
                    ;;
            'java' ) java -jar $ALGO/$ALGO $filepath $OUT_LOCAL;;
        esac
    done < ls $OUT_LOCAL
    echo 'GZIPPING INDIVIDUAL FILES'
    gzip $OUT_LOCAL/*.csv
    echo 'UPLOADING TO S3'
    aws s3 mv $dirname $OUT_BUCKET --recursive
    echo 'CLEANING UP'
    rm -rf $dirname
    rm -rf $OUT_LOCAL
done < list.txt


# ls -d  */ | xargs rm -rf
# ./batch 2>&1 | tee batch.log
# ./batch &> batch.log &
# awk 'NR>37' list.txt
# aws s3 ls s3://pam-data/08-22-2017/ | wc -l 
# Inside single quotes, everything is characters



# aws s3 ls s3://pam-data/08-22-2017/ | tr -s ' ' | cut -f4 -d' ' | grep gt3x | awk 'NR>100 && NR<=150' > list.txt
# wget https://github.com/SPADES-PUBLIC/mHealth-GT3X-converter-public/archive/master.zip && unzip master.zip && mv mHealth-GT3X-converter-public-master/GT3XParser.jar .
# touch batch.sh && chmod +x batch.sh && vim batch.sh